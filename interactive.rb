#encoding: utf-8

require_relative "main.rb"

trap("SIGINT"){print "\n> "}

def make_rational s1, s2
  p = EpiMath::Polynomial.new(splitter(s1))
  q = EpiMath::Polynomial.new(splitter(s2))
  r = EpiMath::Rational.new(p, q)
  return r
end

def interactive
  f_list = []

  print "> "
  loop do
    cmd = gets.to_s.chomp
    cmds = cmd.split

    if cmd.match /^add (\-?)([0-9])+(\|(\-)?([0-9]+))* (\-?)([0-9])+(\|(\-)?([0-9]+))*/
      puts "Add Rational"
      f_list << make_rational(cmds[1], cmds[2])

    elsif cmd.match /^rm [0-9]+/
      puts "Remove Rational"
      f_list[cmds[1].to_i] = nil

    elsif cmd.match /^l(ist)?/
      puts "List Rational"
      puts "Nothing to list" if f_list.size == 0
      f_list.size.times{|i| puts "======#{i}======\n#{f_list[i]}\n"}

    elsif cmd.match /^d(raw)?/
      #create the graph
      g = main_get_gruff 2001
      #get the ylist
      f_list.size.times do |i|
        ylist = []
        x = -10.0

        while x <= 10.0
          ylist << f_list[i].calc(x).round(2)
          x = (x + 0.01).round(2)
        end

        #write them all
        g.data "function(#{i})".to_sym, ylist
      end

      #write the file
      g.write("out.png")
      `eog out.png`

    elsif cmd.match /^export ( .+)/
      name = cmd.sub(/^(export) (.+)/, '\2').to_s
      f_list.size.times do |i|
        xlist = []
        ylist = []
        x = -10.0
        while x <= 10.0
          xlist << x
          ylist << f_list[i].calc(x).round(2)
          x = (x + 0.01).round(2)
        end
        tmp_name = name + "_#{i}.txt"
        puts "Writing " + tmp_name + "..."
        list_to_txt(xlist, ylist, tmp_name)
        puts tmp_name + " wrote."
      end

    elsif cmd.match /^man/
      puts "man"
      exec "man -l ./107transfert.man"

    elsif cmd.match /^h(elp)?/
      puts "Commands :"
      puts "list:  list all rational function registered"
      puts "add:   add a new function"
      puts "rm:    remove a function fom the list, by id"
      puts "draw:  print all function to the screen"
      puts "export:export to a simple txt format"
      puts "help:  print this"
      puts "man:   print the man"
      puts "exit:  exit the program"

    elsif cmd.match /(exit)|(quit)/
      exit

    elsif cmds.size > 0
      puts "'#{cmd}' : Command not found"
    end

    print "> " #if cmds.size > 0
  end
end
